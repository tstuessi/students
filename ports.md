###Port numbers for each student for the ssh connection
For example, audris will use the following setup of .ssh/config (or putty session)
```
host da2
 hostname da2.eecs.utk.edu
 user audris
 port 9101
 LocalForward 8888 127.0.0.1:8888 
```

```
audris use port 9101
```
